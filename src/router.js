import express from 'express'

import GET from './individual_routes/GET.js'; /**To keep our code clean, we import the content of our route as a function
that we will use within the proper route down below */


const router = express.Router()




router.get('/data', GET); 

/**To test this route, first start the server by typing in the console either 'npm start' or 'npm run dev' to use nodemon, it´s advised
 * to use the second 'npm run dev' since the server would restart every time we save any change done to any file within our code,
 * on contrast with 'npm start' you have to stop the server (use Control Key + C), and then run 'npm start' again in the console.
 * Remember, to check that your server is actually running, you should see the console.log() in the console that states: "API server
 * ready on http://localhost3000".
 * 
 * To continue testing the route select GET in Postman, and as URL write 'http://localhost:3000/data' then execute that request
 *  by pressing  "Send" (The blue button) . 
 * 
 * The data that is retrieving is just store in the array called 'data' in 'GET.js' file to keep this basic example simple.
 *  Later on, the data we are using should be coming from other source such as a "file.json" if we are using ´File system´
 *  or a Data Base. The latter should be our preffered method.
 * 
 * If successful the data should display in Postman in the section of  "Response - Body"
 */

export default router