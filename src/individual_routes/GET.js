/**You can 'hard code' more objects into the array keeping the same format and cheking the GET route on Postman,
 * if it´s working correctly, it should be displaying (in Postman) every change you make here. 
*/

let data = [{ 
    "id": "1",
    "name": "name"
},
{
    "id": "2",
    "name": "name2"//would this comment appear on Postman?
}];


const GET = (req, res) => {
    return res.status(200).json(data)
}
/**By writing the GET route with only a return of res (response) with json.(data) (The data we defined within this file), 
 * the only thing this route will do is send whatever we have as "data" in this file to whatever service is requesting it from
 * our server (localhost:300).  This should be tested using Postman as an easy and beginner friendly method of testing.
 */

export default GET; // This export should be imported at 'router.js'

