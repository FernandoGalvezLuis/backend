import express from 'express';

import Routes from './src/router.js'; /** All routes will be kept within router.js file, but we import them to be used here */

const app = express();

const port = 3000; /**Explicitly showing our port is one way of writing code to make this example simple,
but later on, we should keep our 'port' in an '.env' file to secure how the information flows and not to be displayed for 
everyone to see directly. 
Remember files '.env' don´t get uploaded to git, so if you are doing a class project you should leave instructions on what the 
'.env' file should contain to run the project*/

app.use(express.json());

app.use('/', Routes);

app.listen(port, () => console.log(`API server ready on http://localhost:${port}`));
